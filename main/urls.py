from django.urls import path
from .views import index, logout_view,fungsi_data,home


appname = 'main'

urlpatterns = [
    path('', index, name = 'startPage'),
    path('logout/',logout_view, name='logout'),
    path('home/',home,name='home'),
    path('fungsi_data/',fungsi_data,name= 'fungsi_data'),



]
